var Logger = function(min,addOn) {
 
  this.messageType = '';
  this.message = '';
  this.messageNumber = 0;
  this.cookieMinutes = min || 5;
 
  if(addOn){
    this.addOn = addOn;
    this.__proto__[this.addOn] = function (msg) {
      this.output(msg, this.addOn.toUpperCase());
      return this;
    }
  }
 
  this.cookieLife = function () {
    return 60 * 1000 * this.cookieMinutes;
  }
 
  this.storeMessage = function () {
   
    this.outputCookie();
    this.outputConsole();
    this.outputLocalStorage();
    this.messageNumber++;
   
  };
 
  this.setCookieExpiry = function () {
   
    var d = new Date();
    d.setTime(d.getTime() + this.cookieLife());
    return 'expires=' + d.toUTCString() + ';';
   
  };
 
  this.outputCookie = function () {
   
    var cookieName = this.messageType + '_' + this.messageNumber;
    cookieName+= '=' + this.message + ';';
    console.log(document.cookie = cookieName + this.setCookieExpiry());
   
  };
 
  this.outputConsole = function () {
 
    var msgNum = this.messageNumber;
    var messagePrefix = this.messageType + '_' + msgNum + ': ';
    console.log(messagePrefix + this.message);
   
  };
 
  this.outputLocalStorage = function () {
   
    var messagePrefix = this.messageNumber + this.messageType + '_';
    console.log(localStorage[messagePrefix] = this.message);
   
  };
 
  this.output = function (msg,type) {
 
    if(!this.message) this.message = msg;
    this.messageType = type;
    this.storeMessage();
   
  };
 
};
 
Logger.prototype = function() {
 
  var warn = function (msg) {
    this.output(msg,'WARNING');
    return this;
  };
 
  var info = function (msg) {
    this.output(msg,'INFO');
    return this;
  };
 
  var error = function (msg) {
    this.output(msg,'ERROR');
    return this;
  };
 
  return {
    warn: warn,
    info: info,
    error: error
  }
 
}();
 
var logger = new Logger();